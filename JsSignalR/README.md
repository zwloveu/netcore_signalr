# Create Basic Singal Application

## Create web app
```
dotnet new webapp -o SignalRChat
```

## Add Client Site lib
```
dotnet tool install -g Microsoft.Web.LibraryManager.Cli
libman install @microsoft/signalr@latest -p unpkg -d wwwroot/js/signalr --files dist/browser/signalr.js --files dist/browser/signalr.min.js
```

## Add Signal Feature
- add code to Hubs\ChataHub.cs
- config hubfeature to .netcore Startup ConfigServices&Config methods

## Add Signal Client Code
- add code to Pages\Index.cshtml
- add chat.js to www\js