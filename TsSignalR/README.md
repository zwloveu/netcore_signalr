# Create Basic SignalR Web

## Create Web
```
dotnet new web
```

## Enable TypeScript Compile
```
dotnet add package Microsoft.TypeScript.MSBuild
```

## Create `package.json`
- add `"private": true` to the package.json file to prevent installation warnings
```
npm init -y
```

## Install NPM packages
```
npm i -D -E clean-webpack-plugin@3.0.0 css-loader@3.4.2 html-webpack-plugin@3.2.0 mini-css-extract-plugin@0.9.0 ts-loader@6.2.1 typescript@3.7.5 webpack@4.41.5 webpack-cli@3.3.10
```

## Change `package.json`.`scripts` section
```
"scripts": {
  "build": "webpack --mode=development --watch",
  "release": "webpack --mode=production",
  "publish": "npm run release && dotnet publish -c Release"
}
```

## Add `webpack.config.js`
```
const path = require("path");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const { CleanWebpackPlugin } = require("clean-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
module.exports = {
    entry: "./src/index.ts",
    output: {
        path: path.resolve(__dirname, "wwwroot"),
        filename: "[name].[chunkhash].js",
        publicPath: "/"
    },
    resolve: {
        extensions: [".js", ".ts"]
    },
    module: {
        rules: [
            {
                test: /\.ts$/,
                use: "ts-loader"
            },
            {
                test: /\.css$/,
                use: [MiniCssExtractPlugin.loader, "css-loader"]
            }
        ]
    },
    plugins: [
        new CleanWebpackPlugin(),
        new HtmlWebpackPlugin({
            template: "./src/index.html"
        }),
        new MiniCssExtractPlugin({
            filename: "css/[name].[chunkhash].css"
        })
    ]
};
```

## Add `src` folder to contains client assets
```
mkdir src
```

## Add `index.html` under folder `src`

## Add `css` folder under `src` to contains css files

## Add `tsconfig.json` under `src`
```
{
  "compilerOptions": {
    "target": "es5"
  }
}
```

## Add `index.ts` under `src`

## Enalbe SignalR in .netcore
- Create folder `Hubs`
- Add `ChatHub.cs` under `Hubs`
- Config `ChatHub` route in Startup.Configure: endpoints.MapHub<ChatHub>("/hub");.
- Add `AddSignalR` in Startup.ConfigureServices

## Commonication between server and client
- client logic in index.ts
- server logic in Hubs\ChatHub.cs
```
npm i @microsoft/signalr @types/node
```